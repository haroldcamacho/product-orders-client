let mainApp=angular.module('mainApp',['ngRoute']);
let productsById=[];
let productsByName=[];
mainApp.config(['$routeProvider',function($routeProvider){
    $routeProvider
        .when('/home',{
            templateUrl: 'views/home.html',
            controller: 'mainController'
        })
        .when('/requested',{
            templateUrl: 'views/requested.html',
            controller:'requestedController'
        })
        .when('/finished',{
            templateUrl:'views/finished.html',
            controller:'finishedController'
        })
        .when('/newOrder',{
            templateUrl: 'views/new-order.html',
            controller: 'productsController'
        })
        .when('/editOrder/:id',{
            templateUrl: 'views/edit-order.html',
            controller: 'editOrderController'
        })
        .when('/viewOrder/:id',{
            templateUrl: 'views/view-order.html',
            controller: 'editOrderController'
        })        
        .otherwise({
            redirectTo: '/home'
        });
}]);
mainApp.run(function(){
});
mainApp.controller('editOrderController', ['$scope', '$http','$routeParams','$location', function($scope,$http,$routeParams,$location){
    $scope.currentOrderProductsList=[];
    $scope.currentOrder=[];
    $scope.productsListID=[];
    $scope.availableProducts=[];
    const id=$routeParams.id;    
    let reqProducts={
        method: 'GET',
        url: 'https://sisweb-product-orders.herokuapp.com/products',
        headers:{
            User: "343453"
        }
    }
    $scope.productsOrder=[];
    $http(reqProducts).then(function mySuccess(response) {
        $scope.availableProducts=response.data;
        for(let i=0; i<$scope.availableProducts.length;i++){
            productsById[$scope.availableProducts[i]._id]=$scope.availableProducts[i].name;
            productsByName[$scope.availableProducts[i].name]=$scope.availableProducts[i]._id;
        }
      }, 
      function myError(response){
        console.log("ERROR");
    });
    let req={
        method: 'GET',
        url: 'https://sisweb-product-orders.herokuapp.com/order/'+id,
        headers:{
            User: "343453"
        }
    }
    $http(req).then(function mySuccess(response) {
        $scope.currentOrder=response.data; 
        for(let i=0;i<$scope.currentOrder.products.length;i++){
            $scope.currentOrder.products[i]=productsById[$scope.currentOrder.products[i]];
        }
      }, 
      function myError(response){
        console.log("ERROR");
    });
    $scope.removeProduct=function(product){
        var removedProduct=$scope.currentOrder.products.indexOf(product);
        $scope.currentOrder.products.splice(removedProduct,1);
    };
    $scope.addProduct=function(){
        $scope.currentOrder.products.push($scope.selectedProduct.name);
        $scope.selectedProduct=""; 
    };
    $scope.saveOrder=function(){
            for(let i=0;i<$scope.currentOrder.products.length;i++){
                $scope.currentOrder.products[i]=productsByName[$scope.currentOrder.products[i]];
            }
            let newOrder;
            if($scope.clientName){
                newOrder={
                    customer: $scope.clientName,
                    products: $scope.currentOrder.products,
                    status: "draft"
                };    
            }
            else{
                newOrder={
                    customer: $scope.currentOrder.costumer,
                    products: $scope.currentOrder.products,
                    status: "draft"
                };  
            }
            const formattedOrder=angular.toJson(newOrder);
            let req={
                method: 'PUT',
                url: 'https://sisweb-product-orders.herokuapp.com/order/'+id,
                data: formattedOrder,
                headers:{
                    User: "343453"
                }
            }
            $http(req).then(function mySuccess(response) {
                callback(response);
            }, 
            function myError(response){
                console.log("Error");
            });
            $location.path("#!/home");
    }

    $scope.sendToRequested=function(){
        for(let i=0;i<$scope.currentOrder.products.length;i++){
            $scope.currentOrder.products[i]=productsByName[$scope.currentOrder.products[i]];
        }
        let newOrder;
        if($scope.clientName){
             newOrder={
                customer: $scope.clientName,
                products: $scope.currentOrder.products,
                status: "requested"
            };    
        }
        else{
             newOrder={
                customer: $scope.currentOrder.costumer,
                products: $scope.currentOrder.products,
                status: "requested"
            };  
        }
        const formattedOrder=angular.toJson(newOrder);
        let req={
            method: 'PUT',
            url: 'https://sisweb-product-orders.herokuapp.com/order/'+id,
            data: formattedOrder,
            headers:{
                User: "343453"
            }
        }
        $http(req).then(function mySuccess(response) {
            callback(response);
        }, 
          function myError(response){
            console.log("Error");
        });
        $location.path("#!/requested");
    }
}]);
mainApp.controller('mainController', ['$scope', '$http','$route', function($scope,$http,$route){
    $scope.draftOrdersList=[];
    let req={
        method: 'GET',
        url: 'https://sisweb-product-orders.herokuapp.com/orders?status=draft',
        headers:{
            User: "343453"
        }
    }
    $http(req).then(function mySuccess(response) {
        console.log(response.data);
        $scope.draftOrdersList=response.data;
      }, 
      function myError(response){
        console.log("ERROR");
    });
    $scope.deleteOrder=function(draftOrder){
        let orderId=draftOrder._id;
        let delreq={
            method: 'DELETE',
            url: 'https://sisweb-product-orders.herokuapp.com/order/'+orderId,
            headers:{
                User: "343453"
            }
        }
        $http(delreq).then(function mySuccess(response) {
            callback(response);
          }, 
          function myError(response){
            console.log("ERROR");
        });
        $route.reload();
    };
}]);

mainApp.controller('requestedController', ['$scope', '$http', function($scope,$http){
    $scope.requestedOrdersList=[
    ];
    let req={
        method: 'GET',
        url: 'https://sisweb-product-orders.herokuapp.com/orders?status=requested',
        headers:{
            User: "343453"
        }
    }
    $http(req).then(function mySuccess(response) {
        console.log(response.data);
        $scope.requestedOrdersList=response.data;
      }, 
      function myError(response){
        console.log("ERROR");
    });
}]);

mainApp.controller('finishedController', ['$scope', '$http', function($scope,$http){
    $scope.finishedOrdersList=[];
    let req={
        method: 'GET',
        url: 'https://sisweb-product-orders.herokuapp.com/orders?status=finished',
        headers:{
            User: "343453"
        }
    }
    $http(req).then(function mySuccess(response) {
        $scope.finishedOrdersList=response.data;
      }, 
      function myError(response){
        console.log("ERROR");
    });
}]);

mainApp.controller('productsController', ['$scope', '$http','$location', function($scope,$http,$location){
    $scope.productsList=[];
    $scope.availableProducts=[];
    let req={
        method: 'GET',
        url: 'https://sisweb-product-orders.herokuapp.com/products',
        headers:{
            User: "343453"
        }
    }
    $http(req).then(function mySuccess(response) {
        console.log(response.data);
        $scope.availableProducts=response.data;
        for(let i=0;i<$scope.availableProducts.length;i++){
            productsByName[$scope.availableProducts[i].name]=$scope.availableProducts[i]._id;
        }
      }, 
      function myError(response){
        console.log("ERROR");
    });

    $scope.removeProduct=function(product){
        var removedProduct=$scope.productsList.indexOf(product);
        $scope.productsList.splice(removedProduct,1);

    };
    $scope.addProduct=function(){
        $scope.productsList.push($scope.selectedProduct.name);
        $scope.selectedProduct="";
    };
    $scope.saveOrder=function(){
        if($scope.clientName && $scope.productsList>0)
            {
            for(let i=0;i<$scope.productsList.length;i++){
                $scope.productsList[i]=productsByName[$scope.productsList[i]];
            }
            const newOrder={
                customer: $scope.clientName,
                products: $scope.productsList,
                status: "draft"
            };
            const formattedOrder=angular.toJson(newOrder);
            let req={
                method: 'POST',
                url: 'https://sisweb-product-orders.herokuapp.com/order',
                data: formattedOrder,
                headers:{
                    User: "343453"
                }
            }
            $http(req).then(function mySuccess(response) {
                callback(response);
            }, 
            function myError(response){
                console.log("Error");
            });
            $location.path("#!/home");
        }
        else{
            alert("Hay errores en el formulario");
        }
    }

    $scope.sendToRequested=function(){
        if($scope.clientName && $scope.productsList>0)
        {
        for(let i=0;i<$scope.productsList.length;i++){
            $scope.productsList[i]=productsByName[$scope.productsList[i]];
        }
        const newOrder={
            customer: $scope.clientName,
            products: $scope.productsList,
            status: "requested"
        };
        const formattedOrder=angular.toJson(newOrder);
        let req={
            method: 'POST',
            url: 'https://sisweb-product-orders.herokuapp.com/order',
            data: formattedOrder,
            headers:{
                User: "343453"
            }
        }
        $http(req).then(function mySuccess(response) {
          }, 
          function myError(response){
            console.log("Error");
        });
        $location.path("#!/requested");
        }
        else{
            alert("Hay errores en el formulario");
        }
    }   
}]);